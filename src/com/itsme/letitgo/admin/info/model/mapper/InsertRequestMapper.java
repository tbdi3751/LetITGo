package com.itsme.letitgo.admin.info.model.mapper;

import java.util.List;

import com.itsme.letitgo.admin.info.model.dto.InsertRequestDTO;

public interface InsertRequestMapper {

	List<InsertRequestDTO> selectedInsertRequest();

	int detailInfo(InsertRequestDTO insertRequestDTO);

}
