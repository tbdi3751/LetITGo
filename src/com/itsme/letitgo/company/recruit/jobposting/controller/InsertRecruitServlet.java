package com.itsme.letitgo.company.recruit.jobposting.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itsme.letitgo.company.recruit.jobposting.model.dto.RequestJobPostingDTO;
import com.itsme.letitgo.company.recruit.jobposting.model.service.SelectCoMyJobPostingService;


@WebServlet("/recruit/insert")
public class InsertRecruitServlet extends HttpServlet {
       
	// 직무 조회필요, 기술 전체 조회 필요(카테고리를 조회하고 ajax를 통해서  카테고리별 기술 조회 필요), 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SelectCoMyJobPostingService service = new SelectCoMyJobPostingService();
		
		// 직무 전체를 담아올 DTO 인스턴스 생성 후 값 리턴 받음
		Map<String, List<Object>> recruitOption = service.selectRecruitOption();
		
		
		request.setAttribute("jobNameList", recruitOption.get("jobNameList"));
		request.setAttribute("skillsCategoryList", recruitOption.get("skillsCategoryList"));
		request.setAttribute("skillsList", recruitOption.get("skillsList"));
	
		
		String path = "/WEB-INF/views/recruit/insertJobPosting.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
		
		
		
		
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestJobPostingDTO dto = new RequestJobPostingDTO();
		
		// json String으로 받은 값들을 DB로 전달하기 위해 getParameter로 꺼내 DTO의 필드에 값을 저장s하는 과정
		// coMemNo 는 session에 담긴 회원 번호를 가져와야 한다
		request.setCharacterEncoding("UTF-8");
		String[] getSkills = request.getParameterValues("skills");
		
		ArrayList<Integer> skillsList = new ArrayList<>();
			
		
		for(String i: getSkills) {
			skillsList.add(Integer.parseInt(i));
			System.out.println("i" + Integer.parseInt(i));
			
		}
		
		System.out.println("skillsList : " + skillsList);
		
		dto.setCoMemNo(2);
		dto.setJobPostTitle(request.getParameter("jobPostTitle"));
		dto.setJobNo(Integer.parseInt(request.getParameter("jobNo")));
		dto.setJobPostMinExperience(Integer.parseInt(request.getParameter("jobPostMinExperience")));
		dto.setJobPostMaxExperience(Integer.parseInt(request.getParameter("jobPostMaxExperience")));
		dto.setJobPostContents(request.getParameter("jobPostContents"));
		dto.setJobPostDeadLine(java.sql.Date.valueOf(request.getParameter("jobPostDeadLine")));
		dto.setSkillsCodeList(skillsList);
	
		System.out.println("Servlet dto : " + dto);
		
		SelectCoMyJobPostingService service = new SelectCoMyJobPostingService();
		
		boolean result = service.RequestInsertJobPosting(dto);
		
	}

}
