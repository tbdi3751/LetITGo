package com.itsme.letitgo.company.info.model.mapper;

import java.util.List;

import com.itsme.letitgo.company.info.model.dto.CompanyAddInfoDTO;

public interface CompanyTestMapper {

	List<CompanyAddInfoDTO> selectedInfoCompany();

}
