package com.itsme.letitgo.company.regist.model.mapper;

import com.itsme.letitgo.company.regist.model.dto.CoMemberDTO;

public interface ComRegistMapper {

	int coMemberReigst(CoMemberDTO coMemberDTO);

	int coMemberReigstInfo(CoMemberDTO coMemberDTO);
}
