package com.itsme.letitgo.personal.info.model.mapper;

import com.itsme.letitgo.personal.info.model.dto.InfoMemberDTO;

public interface PersonalMemberMapper {

	InfoMemberDTO selectMemberInfo();
	
}
