package com.itsme.letitgo.personal.recruit.jobposting.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itsme.letitgo.personal.recruit.jobposting.model.dto.SelectJobPostingDTO;
import com.itsme.letitgo.personal.recruit.jobposting.model.dto.SelectRequestJobPostingDTO;
import com.itsme.letitgo.personal.recruit.jobposting.model.service.SelectJobPostingService;


@WebServlet("/member/allJobPosting/select")
public class SelectAllJobPostingServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
		SelectRequestJobPostingDTO requestJobPostingDTO = new SelectRequestJobPostingDTO();

	


		SelectJobPostingService selectJobPostingService = new SelectJobPostingService();

		Map<String, List<Object>> jp = selectJobPostingService.selectAllJobPosting();




		request.setAttribute("jobPostingList", jp.get("jpAndInfo"));
		request.setAttribute("jpSkills", jp.get("jpSkills"));
		request.setAttribute("jobNameList", jp.get("jobNameList"));



		String path = "/WEB-INF/views/recruit/jobPostingList.jsp";
		request.getRequestDispatcher(path).forward(request, response);

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}
