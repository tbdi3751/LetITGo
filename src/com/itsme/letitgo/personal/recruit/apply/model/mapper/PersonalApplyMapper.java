package com.itsme.letitgo.personal.recruit.apply.model.mapper;

import java.util.List;
import java.util.Map;

import com.itsme.letitgo.personal.recruit.apply.model.dto.SelectPersonalApplyDTO;

public interface PersonalApplyMapper {

	List<SelectPersonalApplyDTO> selectPersonalApply(/* 개인회원번호 */);

	int updateApplyStatus(Map<String, Object> map);

	int insertApplyStatus(Map<String, Object> map);

	List<SelectPersonalApplyDTO> selectPersonalCanceledApply(/* 개인회원번호 */);

}
